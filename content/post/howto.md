---
title: "How To"
date: 2019-03-04
tags: [ 'howto' ]
categories: [ 'howto' ]
draft: true
---

# The How-To Section

This is just a placeholder page. I'm planning to use this section to add odd
posts when I want to document how I built / fixed / configured something.
