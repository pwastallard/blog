---
title:       "My Strava year - 2020"
subtitle:    ""
description: ""
date:        2020-12-31
author:      Paul
image:       "img/strava_2020.png"
tags:        ["strava"]
---

# My Strava Year - 2020

This is a collage of my Strava year, showing each of the activity maps I posted during the year.
Some of these are short runs, some are longer bike rides.
I wasn't sure how this would turn out, but I think it looks pretty good!

![Strava collage 2020](/img/strava_2020.png)

> The code that generated this image is available on [github](https://github.com/paulstallard/stravatools).
