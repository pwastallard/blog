---
title:       "My Strava year - 2021"
subtitle:    ""
description: ""
date:        2021-12-31
author:      Paul
image:       "img/strava_2021.png"
tags:        ["strava"]
---

# My Strava Year - 2021

As I made one of these last year, I thought I should create my 2021 activity collage!
During 2021 I uploaded at least one activity to Strava every day - there are 413 activities represented in this image.

![Strava collage 2021](/img/strava_2021.png)

> The code that generated this image is available on [github](https://github.com/paulstallard/stravatools).
