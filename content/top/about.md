---
title: "About this site"
date: 2019-04-04T16:01:36+01:00
draft: false
---

# About the site

This site started life as an experiment to play with creating a static website using [Hugo](https://gohugo.io/).
I intend it to host occasional posts about random topics.
Maybe one day it will develop into something more creative.

# About the author

I live in Chichester on the south coast of the UK.
I'm married with two grown up children, both currently working their way through university.

My professional career is documented on Linkedin for anyone interested.
